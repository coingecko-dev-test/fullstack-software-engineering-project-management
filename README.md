# Coingecko Fullstack Engineering Assignment - Project Management

# SECTION 1: Plato - Project Management Software

You are asked to develop a project management software Plato with the following features:

---

## Software Specifications:

### SPEC1: Identities

1. Plato has Users, and Users have project Boards.
2. Plato Users may own individual accounts or belong to an Organization.

### SPEC2: Subscriptions
Plato current provides the following subscription plans:
   1. **Solo**: $ 2.00/mo billed monthly per Board
   2. **Startup** for 5 user licenses: $20.00/mo billed monthly, $18.00/mo billed annually for unlimited Boards. Each additional user seat is $7.00/mo.
   3. **Business** for 20 user licenses: $60.00/mo billed monthly, $55.00/mo billed annually for unlimited Boards. Each additional user seat is $6.00/mo.: 
   
### SPEC3: Projects

Plato Boards are assorted lists of Cards or Lanes with the following attributes:
   1. A title limited to a maximum of 288 characters
   2. A description with unlimited characters
   3. Each Card can have an optional Task List, which consists of a Tasks
       1. A Task has a state of checked/unchecked and has a string attributed limited to 288 chars.
   4. A Task List is `completed` when its Tasks are all checked.
   5. Cards can be colored:
      * A Card with a Task List defaults to `#FF3333` instead. 
      * A Card with a completed Task List has a color of `#3CFF33`.
   6. A User can all Boards that belong to themselves or their Organizations.
   7. When viewing a Board, a User is able to see all Cards with their colors and a **running count of (completed/total) Tasks in a Card** if it has a Task List.


## Submission Guide

Submit a working proof of concept for Plato. Include the following details:

* An ERD of Plato's data models - you may use tools such as [https://dbdiagram.io/](https://dbdiagram.io/) for relational data modeling, or other alternatives.
* An editing and viewing interface for Plato Boards
* Automated tests for the following user experiences: 
   - Alice is a Solo user. Send her an invoice for the last 3 months of usage.
   - Bob is a Startup user with 7 members. Send him an invoice for next 12 months of usage.

#### Scoring Guide:

Candidates will be evaluated based on the following criteria:
 
- **Strategic design patterns** (e.g. [Service Objects](https://www.toptal.com/ruby-on-rails/rails-service-objects-tutorial), [Query Objects](https://martinfowler.com/eaaCatalog/queryObject.html), [Decorators](https://refactoring.guru/design-patterns/decorator)) used in the solution to address extensibility, composability and other challenges.
- **Error and edge-case handling** beyond the user [happy path](http://xunitpatterns.com/happy%20path.html).
- **Automated test coverage**


---

# SECTION 2: Ruby and Rails best practices

1. Given the following code snippet from a simple coin listing application:

```ruby
# app/models/coin_category.rb

class CoinCategory < ApplicationRecord
   ## name                 :string

   has_many :coins

   def published_coins(limit=nil)
      if limit
         coins.where(published: true).limit(limit)
      else
         coins.where(published: true)
      end
   end
end
```
```ruby
# app/models/coin.rb

class Coin < ApplicationRecord
   ## name                 :string
   ## symbol               :string
   ## published            :boolean
   ## price_usd            :float
   ## price_btc            :float
   ## asset_platform_ids   :int[], default([]), is an Array

   BITCOIN_SYMBOL = "₿"

   belongs_to :coin_category
   has_many :news

   def by_author(author_id)
      news.where(author_id: author_id)
   end

end
```
```ruby
# app/models/news.rb

class News < ApplicationRecord
   ## title :string
   ## created_at :datetime

   belongs_to :coin
   belongs_to :author
   scope :by_author, (author_id) -> { where(author_id: author_id)}

end
```
```ruby
# app/controllers/coins_controller.rb

class CoinsController < ApplicationController
   def index
      @coins = Coin.all
   end

   def categories_index
      @categories = Category.all
      @total_market_cap = @categories.published_coins.inject(0) { |sum, coin| sum += coin.price_usd}
   end

   def categories_show
      @category = Category.find(params[:id])
      @category_market_cap = @category.published_coins.inject(0) { |sum, coin| sum += coin.price_usd}

      if @category
         @coins = @category.coins.published_coins(10)
         @news = News.where(coin_id: @coins.pluck(:id))
      else
         render :error
      end
   end

   def show
      @coin = Coin.find(params[:id])
      ...
   end

   def update
      @coin = Coin.find(params[:id])
      @coin.update_attributes(params[:coin])
      ...
   end
end
```
```ruby

# app/views/coins/categories_index.html.slim

| Market Cap:  @total_market_cap

- @coins.each do |coin|
   | #{coin.name}, #{coin.symbol} : #{local_currency(coin.price_usd)} USD, #{coin.price_btc} #{Coin::BITCOIN_SYMBOL}

- @news.each do |news|
   - if news.created_at < Time.now - 1.hour
      | #{news.title}  - #{local_time(news.created_at)}
   - else
      | #{news.title} - NEW!

# app/views/coins/categories_show.html.slim

| Market Cap: #{ local_currency(@category_market_cap)}

- @coins.each do |coin|
   | #{coin.name}, #{coin.symbol} : #{local_currency(coin.price_usd)} USD, #{coin.price_btc} #{Coin::BITCOIN_SYMBOL}

- @news.each do |news|
   - if news.created_at < Time.now - 1.hour
      | #{news.title}  - #{local_time(news.created_at)}
   - else
      | #{news.title} - NEW!

```
```ruby
# app/views/coins/show.html.slim

| #{coin.name}, #{coin.symbol} : #{local_currency(coin.price_usd)} USD, #{coin.price_btc} #{Coin::BITCOIN_SYMBOL}

- @news.each do |news|
   - if news.created_at < Time.now - 1.hour
      = #{news.title}  - #{local_time(news.created_at)} by #{news.author.name}
   - else
      = #{news.title} - NEW! by #{news.author.name}

```


## Submission Guide

Describe a few ways you would improve on the above code implementation. Provide pseudo-code for the **BEFORE** and **AFTER** and elaborate on why.

--- 

# SECTION 3: Rails in Production

1. The front page of Coingecko.com consists of static content and dynamic content in the home page, as illustrated: 

![Question3.png](./assets/question-3.png)

How would you optimize the performance of the above page?

Describe the following: 

* How would you profile the performance of the page? What are the metrics you would use?
* How does Rails work with Javascript and CSS? How does Rails manage client-side and server-side rendered content?
* How would you implement caching for the page? Describe what is cacheable content, different types of caching (fragment caching, low-level caching) in Rails and other caching solutions such edge caching and reverse proxies to improve the page performance.


2. What is a _thread_ in Puma? How do you compare a multi-processes vs multi-threaded web servers? How do you optimize beyond the default recommended `WEB_CONCURRENCY` and `RAILS_MAX_THREADS` parameters for your application?


## Submission Guide

Elaborate on your response with code snippets, benchmarks, illustrations or references as needed. 
Your response need not be exhaustive but can draw from previous experiences or prototypes 
